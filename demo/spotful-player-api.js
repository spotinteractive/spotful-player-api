
/**
 * The Spotful Iframe Interface is a helper library to interface the postmessage interface inorder to receive events and access functions of the player.
 * @param {Object} payload This parameter can accept a string of the dom id reference or the dom reference.
 * @param {String=} id The id used for maintaining parity of the communication.
 */
var SpotfulIframeInterface = function (payload, id) {

    var self = this;

    this.iframe;


    var config = {
        context : 'bespotful',
        version : '0.0.1'
    }

    //Supported events
    var events = {
        "muted" : [],
        "unmuted" : [],
        "ready" : [],
        "play" : [],
        "pause" : [],
        "ended" : [],
        "hotspotClick" : [],
        "timeUpdated" : [],
        "volumeChange" : []
    };

    //Tracking the origin of the child context inorder to whitelist the dns during the postMessage communication. 
    var origin;

    //Stack for tracking our getter requests and distributing up. 
    var getters=[];

    /**
     * The init phase is comprising the aggregated process of instantiation of the object.
     */
    var init = function () {
        
        if(self.isElement(payload)) {

            //let's assign the payload to the iframe.
            self.iframe = payload;

        } else if(typeof payload == 'string') {

            //Let's retrieve the iframe dom and set it to the iframe.
            self.iframe = document.getElementById(payload);

            //Let's check to ensure that the dom element does exists
            if(self.iframe == undefined) {

               throw 'Could not find a DOM element of id '+ payload;

            }

        }

        //Let's check if an id has been provided in the url.
        
        var params = self.getParameters(self.iframe.src);

        if(params['id'] != undefined) {

            //If id is part of the url let's use this as uniq identifier
            config.id = params['id'];

        } else {

            //If not we try to grab the frame id.
            config.id = self.iframe.id;
        
        }

        origin = '*'; //self.iframe.contentWindow.parent.location.href;  //'*'; //self.iframe.src.split('/', 3).join('/');

    }

    /**
     * Helper function for sending a message to the player.
     * @param  {object} payload Prebuilt payload to be sent to the player.
     */
    var send = function (payload) {

        if(typeof payload != 'object') {

            throw "Could not recognize the payload";

        }
        
        payload.context = config.context;
        payload.version = config.version;
        
        self.iframe.contentWindow.postMessage(payload, origin);

    }

    /**
     * Helper function for emitting events.
     * @param  {String} key     The event key of the event to dispatch.
     * @param  {Object} payload The payload to pass when the event fires.
     */
    var emitEvent = function (key, payload) {

        if(events[key] == undefined && typeof events[key] == 'array') {
            return;
        }

        for(var i in events[key]) { 

            events[key][i](payload);
        }

    }

    /**
     * Allows you to register an event handler.
     * @param {String}   key The key of the event to register the handler from.
     * @param {Function} fn  The function that will be fired once the event triggers.
     * @return {Function} The function registered to the event.
     */
    this.addEventListener = function (key, fn) {


        if(events[key] == undefined) {
            throw "Could not recognize event key: " + key;
        }

        if(typeof fn != 'function') {
            throw "Second Parameter must be a function";
        }

        if(events[key].length <= 0) {
            send({
                'method' : 'addEventListener',
                'value' : key,
                'id' : config.id
            });
        }

        events[key].push(fn);

        return fn;
    
    }

    /**
     * Allows you to remove an event handler from the stack.
     * @param  {String}   key The key of the event to register the handler from.
     * @param  {Function} fn  The function that was registered on the event listener.
     */
    this.removeEventListener  = function (key, fn) {

        if(events[key] == undefined) {

            throw "Could not recognize event key : " + key;

        }

        if(typeof fn != 'function') {

            throw "Second Parameter must be a function";

        }

        var index = events[key].indexOf(fn);

        events[key].splice(index, 1);

    }

    /**
     * Helper function for dispatching the a getter request.
     * @param  {String} key     The getter key that fired.
     * @param  {Object} payload The payload of returned message.
     */
    var emitGetter = function(key, payload) {

        if(getters[key] == undefined && typeof getters[key] == 'array') {
            return;
        }

        for(var i in getters[key]) { 

            getters[key][i](payload);
        }


        delete getters[key];

    }

    /**
     * Helper function for building a getter request through the post message interface.
     * @param {String}   key The key of the getter
     * @param {Function} fn  The callback function to fire once the request came back.
     */
    var addGetter = function(key, fn) {

        if(getters[key] == undefined) {
            getters[key] = [];
        }

        if(typeof fn != 'function') {
            throw "Second Parameter must be a function";
        }

        if(getters[key].length <= 0) {
            send({
                'method' : key
            });
        }

        getters[key].push(fn);

        return fn;

    }

    /**
     * Helper function for building a setter request through the post message interface.
     * @param {String} key The key of the setter.
     * @param {String} value The value to set the parameter.
     */
    var addSetter = function(key, value) {

        send({
            'method' : key,
            'value' : value
        });

    }

    /**
     * Accessor for toggling the player to play.
     */
    this.play = function () {

        send({ 'method' : 'play'});

    }

    /**
     * Accessor for toggling the player to pause
     */
    this.pause = function () {

        send({ 'method' : 'pause'});

    }

    /**
     * Accessor for toggling the player to muted/unmuted
     */
    this.mute = function() {
        send({'method': 'mute'});
    }

    this.unmute = function() {
        send({'method': 'unmute'});
    }

    /**
     * Get pause state through the iframe interface.
     * @param {Function} callback The function that will fired once the paused state value is being returned
     */
    this.getPaused = function (callback) {

        addGetter('getPaused', callback);

    }

    /**
     * Get current time through the iframe interface.
     * @param  {Function} callback The function that will be fired once the current time is being returned
     */
    this.getCurrentTime = function (callback) {

        addGetter('getCurrentTime', callback);

    }

    this.setCurrentTime = function (time) {

        addSetter('setCurrentTime', time);
    }

    /**
     * Get duration time through the iframe interface.
     * @param  {Function} callback The function that will be fired once the duration time is being returned.
     */
    this.getDuration = function (callback) {

        addGetter('getDuration', callback);

    }

    /**
     * Get volume through the iframe interface.
     * @param  {Function} callback The function that will be fired once the volume is being returned.
     */
    this.getVolume = function (callback) {

        addGetter('getVolume', callback);

    }

    /**
     * Set volume through the iframe interface.
     * @param {Number} volume The volume level desired to be set.
     */
    this.setVolume = function (volume) {

        addSetter('setVolume', volume);
        
    }

    /**
     * Get loop through the iframe interface.
     * @param  {Function} callback The function that will be fired once the loop is being returned.
     */
    this.getLoop = function (callback) {

        addGetter('getLoop', callback);

    }

    /**
     * Set the base media to be looping or not.
     * @param {boolean} value A boolean that will set the loop state to true or false.
     */
    this.setLoop = function (value) {

        addSetter('setLoop', value);
    }

    /**
     * Provide the width of the main media.
     * @param  {Function} callback The function that will be fired once the width is being returned.
     */
    this.getWidth = function (callback) {

        addGetter('getWidth', callback);
    }   

    /**
     * Provide the height of the main media.
     * @param  {Function} callback The function that will be fired once the height is being returned.
     */
    this.getHeight = function (callback) {

        addGetter('getHeight', callback);
    }

    /**
     * Event handler interfacing with the iframe postmessage event.
     * @param  {Object} e Event object of the iframe message event.
     * @return {undefined}  
     */
    var handleMessage = function(e) {

        if(e.data == undefined) {
            
            console.warn('data is not present in payload');
            return;

        }

        var data;

        //We check if the payload is a string.
        if(typeof e.data == 'string') {

            //If its a string try to parse the string into an object.
            
            try {

                data = JSON.parse(e.data);

            } catch(error) {

                console.warn('Unhandled message!');

            }

        } 
        //We check if the payload is a object
        else if(typeof e.data == 'object') {

            data = e.data;

        }


        ///Let's make sure that the data is object and proceed to parsing and routing.
        if(typeof data != 'object') {
            
            console.warn('Could not handle message.');
            return;
        
        }

        //We check the context of the message to ensure the message is targeted to us.
        if(data.context == undefined || data.context != config.context) {

            console.log('Received a message that is not targeted to us or does not adhere to our API schema.', data);
            return;

        }

        //We check if the message has an id associated to it and check if it matches to our id.
        if(data.id != undefined && data.id != config.id) {
            console.log('ids are not matching');
            return;
        }

       // console.log('New message received');

        //console.log(data);

        ///Lets route those message by method
        
        if(data.event != undefined) {

            emitEvent(data.event, data);

        }

        if(data.method != undefined) {

            emitGetter(data.method, data);

        }
        
    }

    //We instantiate the object
    init();

    //This seems a bit convoluted however it will garantee me that the scope of the window is the proper one.
    this.iframe.contentWindow.parent.addEventListener('message', handleMessage);

};

/**
 *  Enforce method instantiation before the object is populated in its declared window object.
 *  By adding SpotfulIframeInterface.prototype.[methodName] definitions here, the object's methods are injected BEFORE
 *  the SpotfulIframeInterface is evaluated in the parent window context.
 */

SpotfulIframeInterface.prototype.isElement = function(o){
    return (
        typeof HTMLElement === "object" ? o instanceof HTMLElement : 
        o && typeof o === "object" && o !== null && o.nodeType === 1 && typeof o.nodeName==="string"
    );
}


SpotfulIframeInterface.prototype.getParameters = function (url) {

    var paramStart = url.indexOf('?');

    var parametersStr = url.substr(paramStart+1);

    //If string is empty or null return an empty array
    if(parametersStr == null && parametersStr == "") {
        return {};
    }

    var params = {};

    var paramsArray = parametersStr.split("&");

    for(i in paramsArray) {

        var param = paramsArray[i].split("=");

        params[param[0]] = param[1];

    }

    return params;

}
