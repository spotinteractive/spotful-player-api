/* 
    This is the Grunt Workflow for small projects
    Author: Spotful Team
*/

'use strict';

//  Grunt Module
module.exports = function (grunt) {


    // Set a common var for the package.json to parse it later...
    var npmPackage = grunt.file.readJSON('package.json');

    const os = require('os');
    var spofulCredentials = grunt.file.readJSON(os.homedir()+'/.spotful/awsConfiguration.json');


    // Time that shiz
    require('time-grunt')(grunt);

    // Configuration
    grunt.initConfig({

        projectConfig: require('./Grunt.config'),

        // Get Meta Data
        pkg: grunt.file.readJSON('package.json'),

        // Watch for changes in .scss files, & autoprefix them css
        watch: {
            dev: {
                files: '<%= projectConfig.dev %>/**/*.js',
                tasks: ['concat:dev', 'copy:dev']
            },
            static: {
                files: '<%= projectConfig.static %>/**/*',
                tasks : ['copy:static']
            }
        },
        // Concatenation
        concat: {
            options: {
                separator: '\n',
            },
         
            dev: {
                src: ['<%= projectConfig.dev %>/**/*.js'],
                dest: '<%= projectConfig.dist %>/spotful-player-api.js',
            }
        },
        // Copy
        copy: {
            dev : {
                expand: true,
                cwd: '<%= projectConfig.dist %>/',
                src: ['**'],
                dest: '<%= projectConfig.demo %>/'
            },
            static: {
                expand: true,
                cwd: '<%= projectConfig.static %>/',
                src: ['**'],
                dest: '<%= projectConfig.demo %>/'
            },
        },
        // Uglify
        uglify: {
            options: {

                compress: {
                   drop_console: true
                },
                expand: false,
                mangle: false
            },
            dist: {
                files: {
                    '<%= projectConfig.dist %>/spotful-player-api.min.js' : '<%= projectConfig.dev %>/**/*.js'
                }
            }
        },
        // Clean
        // for cleaning up dist directory
        clean: {
            temp: {
                src: '<%= projectConfig.temp %>'
            },
            dist: {
                src: '<%= projectConfig.dist %>'
            }
        },
        
        // Browser Sync Config
        browserSync: {
            dev: {
                bsFiles: {
                    // Refresh on these changes...
                    src: [
                        './src/*',
                        '<%= projectConfig.dist %>/*'
                    ]
                },
                options: {
                    startPath:'demo',
                    watchTask: true,
                    browser: "<%= projectConfig.browser %>",
                    // The index file to serve
                    //proxy: "http://localhost:7000/<%= projectConfig.index %>",
                }
            }
        },

        // AWS S3
        aws_s3: {
            options: {
                accessKeyId: spofulCredentials.accesskeyid, // Use the variables
                secretAccessKey: spofulCredentials.secretAccessKey, // You can also use env variables
                region: 'us-east-1',
                uploadConcurrency: 5, // 5 simultaneous uploads
                downloadConcurrency: 5 // 5 simultaneous downloads
            },
            deploy: {
                options: {
                    bucket: 'spotful-libs',
                    differential: true, // Only uploads the files that have changed
                },
                files: [
                    {expand: true, cwd: 'dist/', src: ['**'], dest: npmPackage.name+'/'+npmPackage.version+'/'}
                ]
            }
        },

        'http-server' : {
     
            dev : {

                root :  '<%= projectConfig.demo %>/',

                port : 3001,

                headers: {
                  "Access-Control-Allow-Origin": "*",
                  "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept"
                }

            }

        },

        concurrent : {

            dev : {
                tasks : [      
                'http-server:dev',
                'browserSync:dev',
                'watch:dev',
                'watch:static'
                ],

                options : {

                    logConcurrentOutput : true
                }
            
            }

        }

    });
    
    // Automatically load Grunt plugins
    require('load-grunt-tasks')(grunt);

    // Register tasks
    grunt.registerTask('dev', [
        'concat:dev',
        'copy:static',
        'copy:dev',
        'concurrent:dev'

    ]);

    grunt.registerTask('build', [
        'clean:dist',
        'concat:dev',
        'copy:static',
        'uglify:dist',
    ]);

    grunt.registerTask('deploy', ['aws_s3']);

    grunt.registerTask("default", ['dev']);

};