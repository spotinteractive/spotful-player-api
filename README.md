# Spotful Player API #

This library provides an api for interfacing the player through the postMessage protocol.

### What is this repository for? ###

* Interfacing a Spotful Player project through the postMessage api.

### How do I get set up? ###

* Clone your repository into your local environement
* Install the required npm packages. *npm install*
* Run *grunt dev* to start developing
* Run *grunt build* to build production ready libraries

#### Developing in SpotfulIframeInterface ####

* You can consult a demo of the postMessage interface through: *http://localhost:3000*
   
   To use the demo properly you need to ensure that the iframe url points to a version of Spotful Player  that contains the *SpotfulPlayerIframeInterface* service.

   You can use the current branch with the implementation *spotful-vpaid-implementation* which resides in `spot-editor`.

* Changes made to index.html and vast.xml files will be automatically persisted to the 'demo/' directory.  Do not make changes in here -- instead, modify the files which sit in the 'static/' directory.

* Changes made to spotfuljs-embed.js and spotfuljs-vpaid.js will be automatically persisted to the 'dist/' directory.  Do not make changes in here -- instead, modify the files which sit in the 'app/' directory.

### Who do I talk to when things go wonky? ###

* Marc-Andre Audet (marc-andre@bespotful.com)
* Spotful Team (dev@bespotful.com)